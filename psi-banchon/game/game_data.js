var PsiGameConstant = {
	checkPoint : [
			{
				"code" : 1,
				"value" : "../resources/cp-content-1.png",
				"descriptionTitle" : "Mức độ hiệu quả",
				"descriptionContent" : "Việc không có thai ngay lúc này quan trọng thế nào với bạn?",
				"answered": false,
				"answerContent" : [ {
					"code" : 1,
					"value" : "Cao nhất",
					"selected" : false
				}, {
					"code" : 2,
					"value" : "Trung bình",
					"selected" : false
				}, {
					"code" : 3,
					"value" : "Thấp",
					"selected" : false
				} ]
			},
			{
				"code" : 2,
				"value" : "../resources/cp-content-2.png",
				"descriptionTitle" : "Yếu tố phiền phức, rắc rối",
				"descriptionContent" : "Các biện pháp tránh thai này sẽ ảnh hưởng đến cuộc sống hàng ngày của bạn như thế nào?",
				"answered": false,
				"answerContent" : [ {
					"code" : 4,
					"value" : "Không hề",
					"selected" : false
				}, {
					"code" : 5,
					"value" : "Một chút",
					"selected" : false
				}, {
					"code" : 6,
					"value" : "Khá phiền phức",
					"selected" : false
				} ]
			},
			{
				"code" : 3,
				"value" : "../resources/cp-content-5.png",
				"descriptionTitle" : "Mức độ kín đáo",
				"descriptionContent" : "Việc chỉ có bạn và bác sĩ của mình biết bạn đang sử dụng biện pháp tránh thai quan trọng như thế nào?",
				"answered": false,
				"answerContent" : [ {
					"code" : 7,
					"value" : "Rất quan trọng",
					"selected" : false
				}, {
					"code" : 8,
					"value" : "Một chút",
					"selected" : false
				}, {
					"code" : 9,
					"value" : "Không quan trọng",
					"selected" : false
				} ]
			},
			{
				"code" : 4,
				"value" : "../resources/cp-content-4.png",
				"descriptionTitle" : "Chu kỳ kinh nguyệt",
				"descriptionContent" : "Sử dụng biện pháp tránh thai có ảnh hưởng thế nào đến chu kỳ kinh nguyệt của bạn?",
				"answered": false,
				"answerContent" : [ {
					"code" : 10,
					"value" : "Không có thay đổi nào",
					"selected" : false
				}, {
					"code" : 11,
					"value" : "Kinh nguyệt ít hơn",
					"selected" : false
				}, {
					"code" : 12,
					"value" : "Hành kinh bất thường",
					"selected" : false
				} , {
					"code" : 13,
					"value" : "Hành kinh nhiều hơn",
					"selected" : false
				}]
			},
			{
				"code" : 5,
				"value" : "../resources/cp-content-3.png",
				"descriptionTitle" : "Khả năng mang thai trở lại",
				"descriptionContent" : "Khi nào thì bạn muốn có thai sau khi ngừng sử dụng các biện pháp tránh thai?",
				"answered": false,
				"answerContent" : [ {
					"code" : 14,
					"value" : "Ngay lập tức",
					"selected" : false
				}, {
					"code" : 15,
					"value" : "Sau vài tháng",
					"selected" : false
				} ]
			} ],
	choice : [ {
		"code" : 1,
		"priority" : 1,
		"score" : 0,
		"name" : "Que cấy tránh thai",
		"link" : "http://banchon.vn/kham-pha/que-cay-tranh-thai",
		"image" : "../resources/results-img-img-pro.png"
	}, {
		"code" : 2,
		"priority" : 1,
		"score" : 0,
		"name" : "Thuốc tiêm tránh thai",
		"link" : "http://banchon.vn/kham-pha/tiem-tranh-thai1",
		"image" : "../resources/results-img-the_shot.png"
	}, {
		"code" : 3,
		"priority" : 2,
		"score" : 0,
		"name" : "Vòng tránh thai",
		"link" : "http://banchon.vn/kham-pha/vong-tranh-thai",
		"image" : "../resources/results-img-iud.png"
	}, {
		"code" : 4,
		"priority" : 3,
		"score" : 0,
		"name" : "Thuốc uống hàng ngày",
		"link" : "http://banchon.vn/kham-pha/vien-uong-hang-ngay",
		"image" : "../resources/results-img-the_pill.png"
	}, {
		"code" : 5,
		"priority" : 4,
		"score" : 0,
		"name" : "Viên tránh thai khẩn cấp",
		"link" : "http://banchon.vn/kham-pha/vien-tranh-thai-khan-cap",
		"image" : "../resources/results-img-emergency_contraception.png" 
	}, {
		"code" : 6,
		"priority" : 5,
		"score" : 0,
		"name" : "Bao cao su",
		"link" : "http://banchon.vn/kham-pha/bao-cao-su",
		"image" : "../resources/results-img-condom.png"
	} ],
	criteria : [[1,0,0,1,0,0,0,1,0,0,1,1,0,1,0]
	           ,[0,1,0,0,1,0,1,0,0,0,0,1,1,0,1]
               ,[1,0,0,1,0,0,1,0,0,0,0,0,1,1,0]
	           ,[0,1,0,0,0,1,0,0,1,0,1,0,0,1,0]
	           ,[0,0,1,0,0,1,1,0,0,0,0,0,0,1,0]
	           ,[0,0,1,0,0,1,0,0,1,1,0,0,0,1,0]],
   gameDescription : [{
	   "state" : "Bước 1:",
	   "value" : "Đi theo đường chỉ dẫn của mũi tên và lựa chọn các yếu tố quan trọng nhất ảnh hưởng đến lựa chọn tránh thai của bạn."
   },
   {
	   "state" : "Bước 2:",
	   "value" : "Chọn mỗi điểm đến trên bản đồ, tương ứng với các vấn đề bạn đang quan tâm. Bạn không nhất thiết phải dừng lại ở tất cả các điểm đến. Đa số phụ nữ đều coi tất cả những yếu tố đều quan trọng, nhưng cũng có những người chỉ tập trung vào một số yêu tố."
   },
   {
	   "state" : "Bước 3:",
	   "value" : "Kết thúc hành trình và bạn sẽ tìm được phương pháp tránh thai phù hợp với mình nhất!"
   }]
	
};
